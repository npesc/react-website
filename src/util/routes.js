import {
    BrowserRouter as Router,
    Routes,
    Route,
  } from "react-router-dom";
import Generator from "../comp/generator";
import Home from "../comp/home";

  

function RoutesList(){ 
    return(
        <Router>
            <Routes>
                <Route exact path="/" element={<Home />} />
                <Route path="/fablan" element={<Generator filename="fablan.md" />} />
            </Routes>
        </Router>
    )
}
export default RoutesList;