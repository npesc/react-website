const error = console.error;


function logError(...parameters) {
    let filter = parameters.find(parameter => {
        return (
        parameter.includes("Warning: %s is deprecated in StrictMode")
        || parameter.includes("Warning:")
        );
    });
    if(!filter) error(...parameters);
}

console.error  = logError;
function ignoreWarns() {
    logError("Warning: render(): Rendering components directly into document.body is discouraged, since its children are often manipulated by third-party scripts and browser extensions. This may lead to subtle reconciliation issues. Try rendering into a container element created for your app.")
}
export default ignoreWarns;