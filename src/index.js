import React from 'react';
import ReactDOM from 'react-dom';
import './assets/index.css';
import RoutesList from './util/routes';


ReactDOM.render(
  <React.StrictMode>
    <RoutesList />
  </React.StrictMode>,
  document.getElementById('root')
);

