import React from "react";
const TypeWriter = ({ content = "", speed = 1000 }) => {
    const [displayedContent, setDisplayedContent] = React.useState("");
    const [index, setIndex] = React.useState(0);
  
    React.useEffect(() => {
      const animKey = setInterval(() => {
        setIndex((index) => {
          if (index >= content.length - 1) {
            clearInterval(animKey);
            return index;
          }
          return index + 1;
        });
      }, speed);
    }, [content.length, speed]);
  
    React.useEffect(() => {
      setDisplayedContent(
        (displayedContent) => displayedContent + content[index]
      );
    }, [content, index]);
  
    return <pre className="type-writer">{displayedContent}</pre>;
  };
  export default TypeWriter;