import React from "react";

export default function Ghbtn() {
    return (
        <>
            <a target="_blank"
            rel="noopener noreferrer"
            href="https://github.com/npesc/">
            Github
            </a>
        </>
    );
}
