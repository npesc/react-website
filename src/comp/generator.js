import React from 'react';
import '../assets/index.css';
import ReactMarkdown from 'react-markdown';
import style from '../assets/markdown-styles.module.css';
import Nav from './nav';

async function loadContents(filename) {
    return await new Promise((resolve, reject) => {
        fetch(filename)
        .then(response => {resolve(response.text());})
        .catch(error => {reject(error);})
    })
}
export default class Generator extends React.Component {
    constructor(props) {
        super(props);
        this.state  = {
            externalData: null,
        }
    }
    componentDidMount() {
        this._asyncRequest = loadContents(this.props.filename).then(
          externalData => {
            this._asyncRequest = null;
            this.setState({externalData});
          }
        );
      }
    render() {
        if (this.state.externalData === null) {
        }
        else  {
        }
        return (
                <React.StrictMode>
                <Nav location={this.props.filename} />
                <ReactMarkdown
                className={style.reactmd}> 
                    {this.state.externalData} 
                </ReactMarkdown>
                </React.StrictMode>
        )
    }
    
}
