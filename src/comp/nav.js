import React from "react";
import Ghbtn from "./ghbtn";
import { Link } from "react-router-dom";

function BackButton() {
    return (
    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor"
    style={{marginLeft:200 , marginTop: -25}}>
     <path fill-rule="evenodd" d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z" clip-rule="evenodd" />
    </svg>
    )
}
export default class Nav extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            path: this.props.location,
        }
    }
    render() {
        if (this.state.path !== '/') {
            return (
            <>
                <div className="menu">
                </div>
                <Link to="/">{BackButton()}</Link>
            </> )
        }
    else {
        return (
        <>
            <div className="menu">
            </div>
      </> )
    }
    }
}