import React from "react";
import Liens from "./liens";
import Projets from "./projets";
import TypeWriter from "./typewriter";


export default class Main extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            path: null,
        }
    }
    render() {
    return (
        <>  
            <h2 style={{ fontSize: "1.8em"}}><TypeWriter content= "Bienvenue" speed={100} /></h2>
                <span> 
                    <div style={{fontWeight: "bold", display: "inline",fontSize: "1.3em"}}>
                        qui suis-je? 
                    </div>
                <br />
                Ahmed, étudiant à <a target="_blank" rel="noopener noreferrer" href="https://www.eurecom.fr">Eurecom</a>
                <br />
                <Liens />   
                </span>
            <br />
                <span>
                    <div style={{fontWeight: "bold", display: "inline",fontSize: "1.3em"}}>
                        projets
                    </div>
                <br />  
                <Projets />
                </span>


        </>
    );
}
}
