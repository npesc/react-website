import React from "react";
import { Link } from 'react-router-dom';

function Projets() {
    return (
    <>
        <Link to="/fablan">/fablan</Link>&nbsp;
        <a target="_blank" rel="noopener noreferrer" href="https://github.com/npesc/quote-script">quote-script</a>
        <br />
        <a target="_blank" rel="noopener noreferrer" href="https://github.com/npesc/connect-4">connect-4</a>
        &nbsp;
        <a target="_blank" rel="noopener noreferrer" href="https://github.com/npesc/mrsync">
        mrsync</a>
        &nbsp;
        <a target="_blank" rel="noopener noreferrer" href="https://github.com/npesc/parcoursup-caml">parcoursup-caml</a>
    </>
    );
}
export default Projets;