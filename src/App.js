import React from 'react';
import './assets/App.css';
import Main from './comp/main';
import DateTime from './comp/time';
import ignoreWarns from './util/ignore-warns';
import Nav from './comp/nav';
function App() {
  ignoreWarns();
   return (
    <div className="App">
      <Nav location={"/"} />
      <div className="App-main">
        <Main />
      </div>
      <div className='time'>
      <a target="_blank" rel="noopener noreferrer" href="https://gitea.com/npesc/react-website">
        source
        </a>
        &nbsp;-&nbsp;
        <DateTime />
      </div>
        {/* <footer>
        <a target="_blank" rel="noopener noreferrer" href="https://gitea.com/npesc/react-website">
        gitea/r-w
        </a>
        &nbsp;/&nbsp; 
        <a target="_blank" rel="noopener noreferrer" href="https://github.com/npesc">
        github/npesc
        </a>
        </footer> */}
    </div>
  );
}
export default App;
