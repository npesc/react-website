# react-website

Code source de mon site personnel.

Base: [create-react-app](https://create-react-app.dev)

## Installation
Il faut avoir installé [yarn](https://yarnpkg.com) au préalable.
```sh
git clone repo_url
yarn install
yarn start
```

#### Auteur: [@npesc](https://gitea.com/npesc)