# FabLAN
Comme son nom l'indique ce projet consiste à faire un réseau en local au Fablab de l'Université Côte d'Azur. Après un grand ménage dans ce dernier, on a récupéré du matériel informatique (switchs, caméras, clients, routeurs, points d'accès wifi, câbles ethernet etc)  pour les réseaux destiné aux entreprises. 

## Matériel utilisé
-  [Netgear RP614v4] - Routeur

- [Cisco Catalyst 2960] - Switch

- DELL Wyse Dx0D, HP Compaq 8000 - Clients

- 2x [Axis 206] - Caméras

- [Câble RJ-45 vers DB-9F] - Pour configurer le switch via la CLI
-  [PuTTY] 
## Diagramme et configuration
![FabLAN Diagram](assets/FablanDG.png "Réseau")

**À suivre...**

[//]: # (Definitions liens)
[Cisco Catalyst 2960]: <https://www.cisco.com/c/en/us/support/switches/catalyst-2960-series-switches/series.html>

[Axis 206]: <https://www.axis.com/products/axis-206>

[Netgear RP614v4]: <https://www.netgear.com/support/product/RP614v4>

[Câble RJ-45 vers DB-9F]: <https://www.cisco.com/c/dam/en/us/support/docs/routers/7000-series-routers/12223-14-i.gif>
[PuTTY]: <https://www.chiark.greenend.org.uk/~sgtatham/putty/>
